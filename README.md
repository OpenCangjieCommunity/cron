<div align="center">
<h1>cron</h1>
</div>

<p align="center">
<img alt="" src="https://img.shields.io/badge/release-v0.0.1-brightgreen" style="display: inline-block;" />
<img alt="" src="https://img.shields.io/badge/build-pass-brightgreen" style="display: inline-block;" />
<img alt="" src="https://img.shields.io/badge/cjc-v0.49.2-brightgreen" style="display: inline-block;" />
<img alt="" src="https://img.shields.io/badge/cjcov-85.6%25-brightgreen" style="display: inline-block;" />
<img alt="" src="https://img.shields.io/badge/project-open-brightgreen" style="display: inline-block;" />
</p>

## 介绍

本项目基于开源库 https://github.com/robfig/cron

cron 为仓颉提供定时任务工具库。

### 特性

- 🚀 cron格式解析
- 🚀 定时任务

### 源码目录

```shell
├── doc
├── src
    ├── cron                    
        ├── chain.cj       
        ├── cron.cj       
        ├── parser.cj       
        ├── pkg_doc.cj       
        ├── spec.cj       
        ├── types.cj       
        ├── waitgroup.cj   
└── test
    ├── DOC                          文档示例
    ├── HLT                          HLT用例
    └── LLT                          LLT自测用例
├── LICENSE
├── module.json
├── README.md
├── README.OpenSource
```

- `DOC` 存放本库使用文档
- `src` 是库源码目录
- `test` 是存放测试用例的文件夹，含有 DOC 功能示例、FUZZ 测试用例、HLT 测试用例、LLT 自测用例

### 接口说明

主要是核心类和成员函数说明,详情见 [API](./doc/feature_api.md)

## <img alt="" src="./doc/assets/readme-icon-compile.png" style="display: inline-block;" width=3%/> 使用说明

### 编译构建

#### linux环境编译

编译描述和具体shell命令

```shell
cjpm build
```

#### Windows环境编译

编译描述和具体cmd命令

```cmd
cjpm build
```


## <img alt="" src="./doc/assets/readme-icon-compile.png" style="display: inline-block;" width=3%/> 相关依赖

- [context](https://gitee.com/HW-PLLab/context)
- [log4cj](https://gitee.com/HW-PLLab/log4cj)


## 开源协议

[MIT License](./LICENSE)

## <img alt="" src="./doc/assets/readme-icon-contribute.png" style="display: inline-block;" width=3%/> 参与贡献

[穗鸿仓](https://gitee.com/organizations/suihongcang)

欢迎给我们提交 PR，欢迎给我们提交 issue，欢迎参与任何形式的贡献。
